Assuming you are in the same directory as this README

## To Clean
ant -buildfile src/build.xml clean

------------------------------------------------------------------------------------
## To Compile
ant -buildfile src/build.xml all


------------------------------------------------------------------------------------
# To Run by specifying arguments from the command line 
	run with args relative to build.xml
ant -buildfile src/build.xml run -Darg0=input_folder/input-1.txt -Darg1=output_folder/output-1.txt -Darg2=4


------------------------------------------------------------------------------------
# To Run by specifying arguments from command Line, where input files are from /home/usr/
	#! replace cameron with your user name, 
	#! change /home/cameron/input_file/input-1.txt with wherever your directory is. 
ant -buildfile src/build.xml run -Darg0=/home/cameron/input_file/input-1.txt -Darg1=/home/cameron/output_file/output-1.txt -Darg2=4


------------------------------------------------------------------------------------
# To run default args 
ant -buildfile src/build.xml run



------------------------------------------------------------------------------------
# To Test
ant -buildfile src/build.xml test



------------------------------------------------------------------------------------
## To Create TarBall for submission 
//unless tar action specified by build.xml
#first clean. 
from parent directory run
tar -cvf cameron_parlman_assign_3.tar cameron_parlman_assign3



------------------------------------------------------------------------------------
## create javadocs 
may not implement. Depends on time. 


------------------------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.â€

[Date: 11/19/2017] -- Please add the date here
Cameron Parlman.


------------------------------------------------------------------------------------
#Data Structures and Complexity in terms of Big O   (Time and Space)


------------------------------------------------------------------------------------
##Citations 
Need to cite the design patterns text used in cs442 at binghamton university. fall 2017 


------------------------------------------------------------------------------------
##Notes while developing 




